package ru.startandroid.nenartovich

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment



class WelcomePage : AppCompatActivity(), FragmentListener {
    private var fragmentNumber: Int = 0

    val SETTINGS = "settings"
    val THEME = "theme"

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        sharedPreferences = getSharedPreferences(SETTINGS, MODE_PRIVATE)
        if (sharedPreferences.getInt(THEME, 1) == 1) {
            setTheme(R.style.AppTheme)
        } else if (sharedPreferences.getInt(THEME, 1) == 2) {
            setTheme(R.style.AppDarkTheme)
        }
        setContentView(R.layout.activity_welcome_page)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.fragment_place, FirstFragment()).commit()
        }
    }


    fun onNextButtonClicked(view: View) {

        if (fragmentNumber == 0) {
            replaceFragment(SecondFragment())
        } else if (fragmentNumber == 1) {
            replaceFragment(ThirdFragment())
        } else if (fragmentNumber == 2) {
            replaceFragment(FourthFragment())
        } else if (fragmentNumber == 3) {
            val intent = Intent()
            intent.setClass(this, LauncherActivity::class.java)
            startActivity(intent)
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        if (fragmentNumber == 3) {
            return
        }
        fragmentNumber++
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_place, fragment)
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.commit()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        fragmentNumber--
    }

    override fun themeChange(value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(THEME, value)
        editor.apply()
        recreate()
    }

}
