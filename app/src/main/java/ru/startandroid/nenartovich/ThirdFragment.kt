package ru.startandroid.nenartovich

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment


class ThirdFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_third, container, false)

        val rb1 = view.findViewById<RadioButton>(R.id.rb1)
        val rb2 = view.findViewById<RadioButton>(R.id.rb2)
        val l1 = view.findViewById<ConstraintLayout>(R.id.topLayout)
        val l2 = view.findViewById<ConstraintLayout>(R.id.bottomLayout)

        val onFirstRadioButtonClick: View.OnClickListener = object : View.OnClickListener {
            override fun onClick(v: View?) {
                rb1.isChecked = true
                rb2.isChecked = false
                (activity as FragmentListener).themeChange(1)
            }
        }

        val onSecondRadioButtonClick: View.OnClickListener = object : View.OnClickListener {
            override fun onClick(v: View?) {
                rb1.isChecked = false
                rb2.isChecked = true
                (activity as FragmentListener).themeChange(2)
            }
        }

        rb1.setOnClickListener(onFirstRadioButtonClick)
        rb2.setOnClickListener(onSecondRadioButtonClick)
        l1.setOnClickListener(onFirstRadioButtonClick)
        l2.setOnClickListener(onSecondRadioButtonClick)
        return view
    }
}
