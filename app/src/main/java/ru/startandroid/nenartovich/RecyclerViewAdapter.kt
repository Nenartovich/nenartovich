package ru.startandroid.nenartovich

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewAdapter(mContext: Context, mData:ArrayList<Book>) : RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    var mContext = mContext
    var mData = mData

    inner class MyViewHolder(itemView: View,
                             tv_book_title: TextView = itemView.findViewById(R.id.book_title_id),
                             color_view: View = itemView.findViewById(R.id.book_img_id))
        : RecyclerView.ViewHolder(itemView) {

        var tv_book_title = tv_book_title
        var color_view = color_view

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        var view: View
        var mInflater: LayoutInflater = LayoutInflater.from(mContext)
        view = mInflater.inflate(R.layout.cardview_item_book, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tv_book_title.setText(mData.get(position).Title)
        holder.color_view.setBackgroundColor(Color.parseColor(mData.get(position).Color))
    }
}