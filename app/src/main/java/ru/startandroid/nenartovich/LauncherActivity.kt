package ru.startandroid.nenartovich

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

class LauncherActivity : AppCompatActivity(), FragmentListener{

    var lsBook: ArrayList<Book> = ArrayList()
    lateinit var sharedPreferences: SharedPreferences
    val SETTINGS = "settings"
    val THEME = "theme"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        sharedPreferences = getSharedPreferences(SETTINGS, MODE_PRIVATE)
        if (sharedPreferences.getInt(THEME, 1) == 1) {
            setTheme(R.style.AppTheme)
        } else if (sharedPreferences.getInt(THEME, 1) == 2) {
            setTheme(R.style.AppDarkTheme)
        }

        setContentView(R.layout.activity_launcher)
        for (i in 1..1000) {
            val newColor: String = generateNewColorName(i)
            lsBook.add(Book(newColor, newColor))
        }

        var myRV: RecyclerView = findViewById(R.id.recyclerview_id)
        var myAdapter: RecyclerViewAdapter = RecyclerViewAdapter(this, lsBook)
        myRV.layoutManager = GridLayoutManager(this, 4)
        myRV.adapter = myAdapter
    }

    fun generateNewColorName( ind: Int): String {
        var color: String = "#"
        while (color.length < 7) {
            color += Random.nextInt(9)
        }
        return color
    }

    override fun themeChange(value: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(THEME, value)
        editor.apply()
        recreate()
    }
}
