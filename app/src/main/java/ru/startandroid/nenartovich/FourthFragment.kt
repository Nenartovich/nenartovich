package ru.startandroid.nenartovich

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.constraintlayout.widget.ConstraintLayout


class FourthFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_fourth, container, false)

        val rb1 = view.findViewById<RadioButton>(R.id.standart_maket_radio_button)
        val rb2 = view.findViewById<RadioButton>(R.id.dense_maket_radio_button)
        val l1 = view.findViewById<ConstraintLayout>(R.id.standart_maket_field)
        val l2 = view.findViewById<ConstraintLayout>(R.id.dense_maket_field)

        val onFirstRadioButtonClick: View.OnClickListener = object : View.OnClickListener {
            override fun onClick(v: View?) {
                rb1.isChecked = true
                rb2.isChecked = false
            }
        }

        val onSecondRadioButtonClick: View.OnClickListener = object : View.OnClickListener {
            override fun onClick(v: View?) {
                rb1.isChecked = false
                rb2.isChecked = true
            }
        }

        rb1.setOnClickListener(onFirstRadioButtonClick)
        rb2.setOnClickListener(onSecondRadioButtonClick)
        l1.setOnClickListener(onFirstRadioButtonClick)
        l2.setOnClickListener(onSecondRadioButtonClick)

        return view
    }

}