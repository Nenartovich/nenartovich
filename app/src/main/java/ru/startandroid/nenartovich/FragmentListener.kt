package ru.startandroid.nenartovich

import androidx.constraintlayout.widget.ConstraintLayout

interface FragmentListener {
    fun themeChange(value: Int)
}